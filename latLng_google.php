<?php
/**
 * Google Maps API Translate Address Into LatLng
 * Tyler Jefford
 * Clique Studios
 * November 2014
 */

//Address to translate into LatLng
$address = '410 S Michigan Ave, Chicago, Illinois, 60605, United States';

//Set up google maps API URL
//Encode address variable
$url = 'http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($address);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$response = json_decode(curl_exec($ch), true);

//If google responds with a status of OK
//Store latitude and longitude into variables
if($response['status'] == "OK"){
  $lat = $response['results'][0]['geometry']['location']['lat'];
	$lng = $response['results'][0]['geometry']['location']['lng'];
}

print('Latitude: '. $lat .'<br />');
print('Longitude: '. $lng);
		    
?>